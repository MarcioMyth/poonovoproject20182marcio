package br.ucsal.bes20182.poo.bank.tui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import br.ucsal.bes20182.poo.bank.domain.Cliente;
import br.ucsal.bes20182.poo.bank.enums.EstadoDaConta;
import br.ucsal.bes20182.poo.bank.persistance.ClienteDAO;
import javax.swing.JTextField;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Toolkit;

public class TransacoesGui {
	static TransacoesGui window = new TransacoesGui();
	static List<Cliente> clientes = ClienteDAO.todosClientes();
	private static int informeConta;
	private JFrame frmMythBank;
	private static JTextField txtCont;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					window.frmMythBank.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public TransacoesGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| javax.swing.UnsupportedLookAndFeelException ex) {
			System.err.println(ex);
		}
		frmMythBank = new JFrame();
		frmMythBank.setIconImage(Toolkit.getDefaultToolkit().getImage(TransacoesGui.class.getResource("/br/ucsal/bes20182/poo/bank/img/BM.jpg")));
		frmMythBank.setTitle("BANCO MYTH");
		frmMythBank.getContentPane().setBackground(new Color(255, 102, 0));
		frmMythBank.setBounds(100, 100, 447, 309);
		frmMythBank.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMythBank.getContentPane().setLayout(null);

		JLabel lblTrasaes = new JLabel("MENU DE TRANSA\u00C7\u00D5ES");
		lblTrasaes.setForeground(Color.WHITE);
		lblTrasaes.setFont(new Font("Monospaced", Font.BOLD, 13));
		lblTrasaes.setBounds(140, 34, 151, 14);
		frmMythBank.getContentPane().add(lblTrasaes);

		JButton btnConsultarSaldo = new JButton("Consultar Saldo");
		btnConsultarSaldo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { // Consultar Saldo

				informeConta = Integer.parseInt(txtCont.getText());
				
				for (Cliente cliente : clientes) {
					if (informeConta == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVA) {

						JOptionPane.showMessageDialog(null, "Seu saldo �: " + cliente.getSaldo(),"SALDO",JOptionPane.INFORMATION_MESSAGE);
						break;
					}else {
					
						
				
				}
				}		

			}
		});
		btnConsultarSaldo.setBounds(140, 116, 137, 23);
		frmMythBank.getContentPane().add(btnConsultarSaldo);

		JButton btnRealizarDepsito = new JButton("Realizar Dep\u00F3sito");
		btnRealizarDepsito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {// Realizar Deposito

				informeConta = Integer.parseInt(txtCont.getText());

				for (Cliente cliente : clientes) {
					if (informeConta == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVA ) {
						JOptionPane.showMessageDialog(null, "Seu saldo �: " + cliente.getSaldo(),"SALDO",JOptionPane.INFORMATION_MESSAGE);

						int depositarDinheiro = Integer
								.parseInt(JOptionPane.showInputDialog("Informe o Valor a ser Depositado: "));
						cliente.setSaldo(depositarDinheiro + cliente.getSaldo());
						JOptionPane.showMessageDialog(null,"VALOR DEPOSITADO "+depositarDinheiro, "SUCESSO!",JOptionPane.INFORMATION_MESSAGE);
						break;

						
					}else {
					
				}
				}

				
				

			}
		});
		btnRealizarDepsito.setBounds(140, 140, 137, 23);
		frmMythBank.getContentPane().add(btnRealizarDepsito);

		JButton btnNewButton = new JButton("Realizar Saque");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {// Realizar Saque
				informeConta = Integer.parseInt(txtCont.getText());

				for (Cliente cliente : clientes) {
					if (informeConta == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVA) {
						JOptionPane.showMessageDialog(null, "Seu saldo �: " + cliente.getSaldo());
						int sacarDinheiro = Integer
								.parseInt(JOptionPane.showInputDialog("Informe o Valor a ser Sacado: "));

						if (cliente.getSaldo() >= sacarDinheiro) {
							cliente.setSaldo(cliente.getSaldo() - sacarDinheiro);
							JOptionPane.showMessageDialog(null,"VALOR RETIRADO "+sacarDinheiro, "SUCESSO!",JOptionPane.INFORMATION_MESSAGE);
							break;
						}else {
							JOptionPane.showMessageDialog(null, "Voc� n�o possu� tanto dinheiro! ","ERRO",JOptionPane.ERROR_MESSAGE);
							break;
						}

					}else {
					
					}

				}

				
				
			}

		});
		btnNewButton.setBounds(140, 163, 137, 23);
		frmMythBank.getContentPane().add(btnNewButton);

		JButton btnMenuPrincipal = new JButton("Menu Principal");
		btnMenuPrincipal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				window.frmMythBank.setVisible(false);
				window = new TransacoesGui();
				MenuGui.main(null);
			}
		});
		btnMenuPrincipal.setBounds(140, 197, 137, 23);
		frmMythBank.getContentPane().add(btnMenuPrincipal);
		
		JLabel lblInformeSuaConta = new JLabel("Informe sua conta:");
		lblInformeSuaConta.setForeground(Color.WHITE);
		lblInformeSuaConta.setBounds(96, 74, 124, 14);
		frmMythBank.getContentPane().add(lblInformeSuaConta);
		
		txtCont = new JTextField();
		txtCont.setBounds(200, 71, 92, 20);
		frmMythBank.getContentPane().add(txtCont);
		txtCont.setColumns(10);
	}

	public static void transacoes() {

		if (clientes.size() == 0) {
			JOptionPane.showMessageDialog(null, "N�O H� ClIENTES DISPON�VEIS!","ERRO",JOptionPane.ERROR_MESSAGE);
			
		}

	}
}
