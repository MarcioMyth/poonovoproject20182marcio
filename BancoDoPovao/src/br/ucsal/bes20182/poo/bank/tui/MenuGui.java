package br.ucsal.bes20182.poo.bank.tui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import br.ucsal.bes20182.poo.bank.domain.Cliente;
import br.ucsal.bes20182.poo.bank.enums.EstadoDaConta;
import br.ucsal.bes20182.poo.bank.persistance.ClienteDAO;
import java.awt.Toolkit;

public class MenuGui {
static MenuGui window = new MenuGui();
	private JFrame frmBancoMyth;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					window.frmBancoMyth.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public MenuGui() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| javax.swing.UnsupportedLookAndFeelException ex) {
			System.err.println(ex);
		}
		frmBancoMyth = new JFrame();
		frmBancoMyth.setIconImage(Toolkit.getDefaultToolkit().getImage(MenuGui.class.getResource("/br/ucsal/bes20182/poo/bank/img/BM.jpg")));
		frmBancoMyth.setTitle("BANCO MYTH");
		frmBancoMyth.getContentPane().setBackground(new Color(255, 102, 0));
		frmBancoMyth.setBounds(100, 100, 450, 300);
		frmBancoMyth.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBancoMyth.getContentPane().setLayout(null);

		JLabel txtBoasVindas = new JLabel("BEM VINDO AO BANCO MYTH");
		txtBoasVindas.setFont(new Font("Monospaced", Font.BOLD, 15));
		txtBoasVindas.setForeground(Color.WHITE);
		txtBoasVindas.setBackground(Color.WHITE);
		txtBoasVindas.setBounds(119, 34, 211, 14);
		frmBancoMyth.getContentPane().add(txtBoasVindas);

		JLabel lblOQueDeseja = new JLabel("O QUE DESEJA?");
		lblOQueDeseja.setBackground(Color.WHITE);
		lblOQueDeseja.setForeground(Color.WHITE);
		lblOQueDeseja.setBounds(58, 65, 127, 14);
		frmBancoMyth.getContentPane().add(lblOQueDeseja);

		JButton btnTrandaes = new JButton("Transa\u00E7\u00F5es");
		btnTrandaes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.frmBancoMyth.setVisible(false);
				TransacoesGui.main(null);
				

			}
		});
		btnTrandaes.setBounds(159, 123, 127, 23);
		frmBancoMyth.getContentPane().add(btnTrandaes);

		JButton btnAbrirConta = new JButton("Abrir Conta");
		btnAbrirConta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.frmBancoMyth.setVisible(false);
				CadastrarGui.main(null);
			}
		});
		btnAbrirConta.setBounds(159, 90, 127, 23);
		frmBancoMyth.getContentPane().add(btnAbrirConta);

		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.frmBancoMyth.setVisible(false);
				listarCliente();
				MenuGui.main(null);
			}
		});
		btnConsultar.setBounds(159, 157, 127, 23);
		frmBancoMyth.getContentPane().add(btnConsultar);

		JButton btnRestrito = new JButton("Restrito");
		btnRestrito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int tentativa = 0;
				String senha = JOptionPane.showInputDialog(null, "SENHA DE ACESSO: ","PASSWORD",JOptionPane.WARNING_MESSAGE);
				if (tentativa > 2) {
					JOptionPane.showMessageDialog(null,"Voc� atingiu o limite de tentativas.","BLOQUEADO",JOptionPane.ERROR_MESSAGE);

				}
				if (senha.equals(MenuTui.SENHA_FUNCIONARIO) && tentativa <= 2) {
					tentativa = 0;
					window.frmBancoMyth.setVisible(false);
					FuncionarioGui.main(null);
				}else {
					JOptionPane.showMessageDialog(null,"Senha Inv�lida! ","Tente Novamente",JOptionPane.ERROR_MESSAGE);
					tentativa++;
				}
			}
		});
		btnRestrito.setBounds(159, 191, 127, 23);
		frmBancoMyth.getContentPane().add(btnRestrito);

		JButton btnSair = new JButton("Voltar");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.frmBancoMyth.setVisible(false);
				BankGui.main(null);
			}
		});
		btnSair.setBounds(335, 227, 89, 23);
		frmBancoMyth.getContentPane().add(btnSair);
	}

	public static void listarCliente() {

		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			JOptionPane.showMessageDialog(null, "SEM CONTAS DISPON�VEIS!","ERRO",JOptionPane.ERROR_MESSAGE);
			window.frmBancoMyth.setVisible(false);
			MenuGui.main(null);
		}
		for (Cliente cliente : clientes) {
			if (cliente.getEstado() == EstadoDaConta.BLOQUEADA) {
				JOptionPane.showMessageDialog(null, "CONTA DO CLIENTE: " + cliente.getConta() + ": " + cliente.getNome()
						+ " BLOQUEADA COM SUCESSO!","SUCESSO",JOptionPane.INFORMATION_MESSAGE);
				window.frmBancoMyth.setVisible(false);
				MenuGui.main(null);

			} else if (cliente.getEstado() == EstadoDaConta.ENCERRADA) {
				JOptionPane.showMessageDialog(null,
						"A Conta: " + cliente.getConta() + " foi encerrada com sucesso!" + cliente.getConta(),"SUCESSO",JOptionPane.INFORMATION_MESSAGE);
				window.frmBancoMyth.setVisible(false);
				MenuGui.main(null);
			} else {
				JOptionPane.showMessageDialog(null,
						 "\nNome: " + cliente.getNome() + "\nTelefone: "
								+ cliente.getTelefone() + "\nCPF: " + cliente.getCpf() + "\nCEP: " + cliente.getCep()
								+ "\nRenda: " + cliente.getRendaSalarial() + "\nRG: " + cliente.getRg() + "\nConta: "
								+ cliente.getConta() + "\nSaldo: " + cliente.getSaldo()+ "\nEstado da Conta: " + cliente.getEstado(), "CONTA " + cliente.getConta(),JOptionPane.DEFAULT_OPTION);
			}
		}

	}
}
