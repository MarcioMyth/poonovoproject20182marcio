package br.ucsal.bes20182.poo.bank.tui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Toolkit;

public class BankGui {

	private JFrame frmBancoMyth;
	static BankGui window = new BankGui();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window.frmBancoMyth.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public BankGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| javax.swing.UnsupportedLookAndFeelException ex) {
			System.err.println(ex);
		}
		frmBancoMyth = new JFrame();
		frmBancoMyth.setIconImage(Toolkit.getDefaultToolkit().getImage(BankGui.class.getResource("/br/ucsal/bes20182/poo/bank/img/BM.jpg")));
		frmBancoMyth.setTitle("BANCO MYTH");
		frmBancoMyth.getContentPane().setBackground(new Color(255, 102, 0));
		frmBancoMyth.setBounds(100, 100, 450, 300);
		frmBancoMyth.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBancoMyth.getContentPane().setLayout(null);

		JLabel lblBemVindoAo = new JLabel(" BANCO MYTH");
		lblBemVindoAo.setForeground(new Color(255, 255, 255));
		lblBemVindoAo.setFont(new Font("Segoe UI Black", Font.BOLD, 16));
		lblBemVindoAo.setBounds(147, 38, 144, 73);
		frmBancoMyth.getContentPane().add(lblBemVindoAo);

		JButton btnMenu = new JButton("ENTRAR");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 window.frmBancoMyth.setVisible(false);
				MenuGui.main(null);

			}

		});
		btnMenu.setBounds(169, 144, 89, 23);
		frmBancoMyth.getContentPane().add(btnMenu);

		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.exit(0);
			}
		});
		btnSair.setBounds(335, 227, 89, 23);
		frmBancoMyth.getContentPane().add(btnSair);

		JLabel lblByMrcioJnior = new JLabel("By: M\u00E1rcio J\u00FAnior");
		lblByMrcioJnior.setForeground(Color.WHITE);
		lblByMrcioJnior.setBounds(239, 88, 102, 14);
		frmBancoMyth.getContentPane().add(lblByMrcioJnior);

	}

}
