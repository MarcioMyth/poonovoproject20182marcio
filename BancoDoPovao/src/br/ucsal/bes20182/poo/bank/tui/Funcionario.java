package br.ucsal.bes20182.poo.bank.tui;

import java.util.List;

import br.ucsal.bes20182.poo.bank.domain.Cliente;
import br.ucsal.bes20182.poo.bank.enums.EstadoDaConta;
import br.ucsal.bes20182.poo.bank.persistance.ClienteDAO;

public class Funcionario {
	public static void acessoFuncionario() {
		System.out.println("Digite a senha de admin:  ");
		String tentativaSenha = MenuTui.sc.nextLine();
		if (MenuTui.tentativa > 2) {
			System.out.println("Voc� atingiu o limite de tentativas.");
			MenuTui.menu();
		}
		if (tentativaSenha.equals(MenuTui.SENHA_FUNCIONARIO) && MenuTui.tentativa <= 2) {
			MenuTui.tentativa = 0;
			System.out.println("AREA RESTRITA(FUNCION�RIO)");
			System.out
					.println("(1) BLOQUEAR \n(2) DESBLOQUEAR \n(3) ENCERRAR CLIENTE \n(4) ADICIONAR SALDO \n(5) SAIR");
			int escolha = MenuTui.sc.nextInt();

			switch (escolha) {
			case 1:
				bloquearCliente();

				break;

			case 2:
				desbloquearCliente();
				break;

			case 3:
				encerrarCliente();
				break;

			case 4:
				adicionarSaldo();
				break;

			case 5:
				MenuTui.menu();
			}
		} else {

			System.out.println("Senha Inv�lida! ");

			MenuTui.tentativa++;
			MenuTui.menu();
		}

	}

	public static void adicionarSaldo() {
		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			System.out.println("N�o h� clientes!");
			MenuTui.menu();
		}
		System.out.println("Clientes e Saldos: ");
		for (Cliente cliente : clientes) {
			if (cliente.getEstado() == EstadoDaConta.ATIVA) {
				System.out.println("Nome: " + cliente.getNome() + " Saldo: " + cliente.getSaldo() + " Conta: "
						+ cliente.getConta());
			}
		}
		System.out.println("Informe pela conta qual cliente deseja modificar o saldo: ");
		int escolhaConta = MenuTui.sc.nextInt();
		for (Cliente cliente2 : clientes) {
			if (cliente2.getConta() == escolhaConta && cliente2.getEstado() == EstadoDaConta.ATIVA) {
				System.out.println("Informe o saldo: ");
				cliente2.setSaldo(MenuTui.sc.nextDouble());
				MenuTui.menu();
			}
		}
		MenuTui.menu();

	}

	public static void desbloquearCliente() {
		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			System.out.println("N�o h� clientes!");
			MenuTui.menu();
		}

		System.out.println("Qual dos clientes deseja desbloquear? ");

		for (Cliente cliente : clientes) {
			if (cliente.getEstado() == EstadoDaConta.BLOQUEADA) {
				System.out.println("\nCLIENTE #" + cliente.getConta());
				System.out.println();
				System.out.println("\tNome do cliente: " + cliente.getNome() + "\tNumero da conta do cliente: "
						+ cliente.getConta());
				System.out.println("EST� BLOQUEADO!");
			} else {
				System.out.println("\nCLIENTE #" + cliente.getConta());
				System.out.println("\tNome do cliente: " + cliente.getNome() + "\tNumero da conta do cliente: "
						+ cliente.getConta());
				System.out.println("EST� ATIVO!");
			}
		}

		System.out.println("Informe o n�mero da conta referente ao cliente que desej�s desbloquear: ");
		int escolhaDesbloquear = MenuTui.sc.nextInt();

		for (Cliente cliente : clientes) {
			if (escolhaDesbloquear == cliente.getConta() && cliente.getEstado() == EstadoDaConta.BLOQUEADA) {
				cliente.setEstado(EstadoDaConta.ATIVA);
				System.out.println("O cliente " + cliente.getNome() + " foi desbloqueado!");
				MenuTui.menu();
			} else {
				System.out.println("N�o h� cliente com n�mero da conta correspondente que possa ser bloqueado.");
				MenuTui.menu();
			}
		}
	}

	public static void encerrarCliente() {
		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			System.out.println("N�o h� clientes!");
			MenuTui.menu();
		}

		System.out.println("Qual dos clientes deseja encerrar? ");

		for (Cliente cliente : clientes) {
			if (cliente.getEstado() == EstadoDaConta.BLOQUEADA) {
				System.out.println("\nCLIENTE #" + cliente.getConta());
				System.out.println();
				System.out.println("\tNome do cliente: " + cliente.getNome() + "\tNumero da conta do cliente: "
						+ cliente.getConta());
				System.out.println("EST� BLOQUEADO!");
			} else if (cliente.getEstado() == EstadoDaConta.ATIVA) {
				System.out.println("\nCLIENTE #" + cliente.getConta());
				System.out.println("\tNome do cliente: " + cliente.getNome() + "\tNumero da conta do cliente: "
						+ cliente.getConta());
				System.out.println("EST� ATIVO!");
			}
		}

		System.out.println("Informe o n�mero da conta referente ao cliente que desej�s encerrar: ");
		int escolhaDesbloquear = MenuTui.sc.nextInt();

		for (Cliente cliente : clientes) {
			if (escolhaDesbloquear == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVA
					|| cliente.getEstado() == EstadoDaConta.BLOQUEADA && cliente.getSaldo() < 1) {
				cliente.setEstado(EstadoDaConta.ENCERRADA);
				System.out.println("O cliente " + cliente.getNome() + " foi encerrado!");
				MenuTui.menu();
			} else if (cliente.getSaldo() > 1) {
				System.out.println("S� � poss�vel encerrar contas sem saldo, favor realize o saque dos "
						+ cliente.getSaldo() + "R$ restantes!");
				MenuTui.menu();
			} else {
				System.out.println("N�o h� cliente com n�mero da conta correspondente que possa ser encerrado.");
				MenuTui.menu();
			}
		}
	}

	public static void bloquearCliente() {
		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			System.out.println("N�o h� clientes!");
			MenuTui.menu();
		}

		System.out.println("Qual dos clientes deseja bloquear? ");

		for (Cliente cliente : clientes) {
			System.out.println("\nCLIENTE #" + cliente.getConta());
			System.out.println();
			System.out.println(
					"\tNome do cliente: " + cliente.getNome() + "\tNumero da conta do cliente: " + cliente.getConta());
		}
		System.out.println("Informe o n�mero da conta referente ao cliente que desej�s bloquear: ");
		int escolhaBloquear = MenuTui.sc.nextInt();

		for (Cliente cliente : clientes) {
			if (escolhaBloquear == cliente.getConta()) {
				cliente.setEstado(EstadoDaConta.BLOQUEADA);
				System.out.println("O cliente " + cliente.getNome() + " foi bloqueado!");
				MenuTui.menu();
			} else {
				System.out.println("N�o h� cliente com n�mero da conta correspondente que possa ser bloqueado.");
				MenuTui.menu();
			}
		}

	}

}
