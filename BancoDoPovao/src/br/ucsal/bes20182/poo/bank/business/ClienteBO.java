package br.ucsal.bes20182.poo.bank.business;

import br.ucsal.bes20182.poo.bank.domain.Cliente;
import br.ucsal.bes20182.poo.bank.persistance.ClienteDAO;

public class ClienteBO {

	ClienteBO clienteBO = new ClienteBO();

	public static String validarCliente(Cliente cliente) {

		if (validar(cliente) == null) {
			ClienteDAO.addCliente(cliente);
			return null;
		}

		return validar(cliente);

	}

	private static String validar(Cliente cliente) {
		if (cliente.getNome().trim().isEmpty()) {
			return "Nome do contato n�o informado.";
		}
		if (cliente.getTelefone().trim().isEmpty()) {
			return "Telefone do contato n�o informado.";
		}
		if (cliente.getCpf().trim().isEmpty()) {
			return "Cpf do contato n�o informado.";
		}
		if (cliente.getRg().trim().isEmpty()) {
			return "RG do contato n�o informado.";
		}
		
		return null;
	}
}
